//var $ = jQuery.noConflict();
var carouselactive = true;
var main_file = [];
var main_detailed = [];
var not_found = [];
var definitive = [];

$("#btnsubmit").click(function(e)	{
	e.preventDefault();
	//var _ccursor = ReadMasterList();
	//ReadChildList(_ccursor);
	console.log(main_file.length);
});
/*
$("#btnaddissue").click(function(e)	{
	e.preventDefault();
	ReadMasterList();
});
$("#btncancel").click(function(e)	{
	e.preventDefault();
	validate();
});*/
$("#master_file").change(function(e) {
	ReadMasterList();

});
$("#list_file").change(function(e) {
	ReadChildList();
});

function check(fileUpload) {
	var lines=fileUpload.split("\n");
	  var result = [];
	  var headers=lines[0].split("\t");
	 	 console.log(headers);
	  for(var i=1;i<lines.length;i++){
		  var obj = {};
		  var currentline=lines[i].split("\t");
		  for(var j=0;j<headers.length;j++){
			  obj[headers[j]] = currentline[j];
		  }
		  result.push(obj);
	  }
	  console.log(result);
	  //return result; //JavaScript object
	return JSON.stringify(result); //JSON
}

function ReadMasterList(fileUpload) {
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    if (regex.test($("#master_file").val().toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var rows = e.target.result.split("\r\n");
                var current = 0;
                var fheader = (rows[0].split("\t"));
                main_file = rows;
                createHeaders(fheader);
                /*for (var i = 0; i < fheader.length; i++) {
                	//console.log(fheader[i]);
                }*/
            }
            reader.readAsText($("#master_file")[0].files[0]);
        } else {
            alert("This browser does not support HTML5.");
        }

    } else {
        alert("Please upload a valid tab delimited file.");
    }

}

function createHeaders(headers_List){   
	var select_Content="";
	for (var i = 0; i < headers_List.length; i++) { 
        select_Content += '<option value="'+i+'">'+headers_List[i]+'</option>';
       // console.log(headers_List[i]);
    }
    $("#main_headers").empty();
    $("#main_headers").append('<div class="select_Header">Location Name: <select class="select_header_input" id="select_loc_name">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location State: <select class="select_header_input" id="select_loc_state">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Store ID: <select class="select_header_input" id="select_loc_id">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location Zip: <select class="select_header_input" id="select_loc_zip">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location URL: <select class="select_header_input" id="select_loc_url">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location Phone: <select class="select_header_input" id="select_loc_phone">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location Address: <select class="select_header_input" id="select_loc_address">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location City: <select class="select_header_input" id="select_loc_city">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location Country: <select class="select_header_input" id="select_loc_country">'+select_Content+'</select>	</div>');
    $("#main_headers").append('<div class="select_Header">Location Email: <select class="select_header_input" id="select_loc_email">'+select_Content+'</select>	</div>');

	document.getElementById("select_loc_name").value = 0;
	document.getElementById("select_loc_state").value = 1;
	document.getElementById("select_loc_id").value = 10;
	document.getElementById("select_loc_zip").value = 15;
	document.getElementById("select_loc_url").value = 16;
	document.getElementById("select_loc_phone").value = 17;
	document.getElementById("select_loc_address").value = 18;
	document.getElementById("select_loc_city").value = 19;
	document.getElementById("select_loc_country").value = 20;
	document.getElementById("select_loc_email").value = 21;
   //	console.log(select_Content);
}

function ReadChildList(fileUpload) {

	if (main_file.length <= 0) {
		alert("Please select a valid Master Sheet");
		return ;
	}

	var heads_field_name = $("#select_loc_name").val();
	var heads_field_state = $("#select_loc_state").val();
	var heads_field_id = $("#select_loc_id").val();
	var heads_field_zip = $("#select_loc_zip").val();
	var heads_field_url = $("#select_loc_url").val();
	var heads_field_phone = $("#select_loc_phone").val();
	var heads_field_address = $("#select_loc_address").val();
	var heads_field_city = $("#select_loc_city").val();
	var heads_field_country = $("#select_loc_country").val();
	var heads_field_email = $("#select_loc_email").val();


	for (var i = 0; i < main_file.length; i++) {
		var temp = main_file[i].split("\t");
		main_detailed[i] = temp[heads_field_id];//.split(",");
	}

    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.txt)$/;
    if (regex.test($("#list_file").val().toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
               
                var rows = e.target.result.split("\r\n");
                var current = 0;
                //console.log(main_file);
                for (var i = 0; i < rows.length; i++) {
                    var cells = rows[i].split(",");
                    var id ="";
                    current ++;
                    var cellis = rows[i].split("	");
                	var storeid=cellis[2];
                	_posicion = main_detailed.indexOf(storeid);
                	//var _line=[];
                	if (_posicion >=0) {
                    	temp = (main_file[_posicion]).split("\t");
                    	var _line = [cellis[0], cellis[1],cellis[2],temp[heads_field_name], temp[heads_field_state], temp[heads_field_id], temp[heads_field_zip], temp[heads_field_url], temp[heads_field_phone], temp[heads_field_address], temp[heads_field_city], temp[heads_field_country], temp[heads_field_email]];
                    	definitive[i] = _line;

                    } else {
                    	cant=not_found.length;
                    	not_found[cant+1] = storeid;
                    	//console.log("not_found");
                    	//console.log(not_found);
                    }
                    //console.log(definitive);

                }
				var csv = definitive.map(function(d){
				   return JSON.stringify(d);
				})
				.join('\n') 
				.replace(/(^\[)|(\]$)/mg, '');
				console.log(csv);

					        var uri = "data:text/csv;charset=utf8," + encodeURIComponent(csv);
					        var fileName = "data.csv";

					        //$("#main_div_container").append('<a href="" id="link">Export File</a>');
					       	//console.log(link);
					        var link = document.createElement("a");
					        if (link.download !== undefined) { // feature detection
					            // Browsers that support HTML5 download attribute
					            link.setAttribute("href", uri);
					            link.setAttribute("download", fileName);
					        }
					        else if (navigator.msSaveBlob) { // IE 10+
					            link.addEventListener("click", function (event) {
					                var blob = new Blob([csv], {
					                    "type": "text/csv;charset=utf-8;"
					                });
					                navigator.msSaveBlob(blob, fileName);
					            }, false);
					        }
					        else {
					            // it needs to implement server side export
					            link.setAttribute("href", "http://www.example.com/export");
					        }
					        link.innerHTML = "Download File";
					        $("#main_div_container").append('<div class="separatordiv"></div>');
					        var div = document.getElementById("main_div_container");
					        div.appendChild(link);
					        //document.body.appendChild(link);
					        htmltext= "<div>";
					        var unique = not_found.filter(function(elem, index, self) {
							    return index == self.indexOf(elem);
							})
					        for (var i = 0; i <=unique.length; i++) {
					        	htmltext += unique[i]+"<br />";
					        }
					        htmltext += "</div>";
					        $("#main_div_container").append(htmltext);

            }
            reader.readAsText($("#list_file")[0].files[0]);
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid tab delimited file.");
    }
}