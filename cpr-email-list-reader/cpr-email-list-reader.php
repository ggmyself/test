<?php
/*
Plugin Name: CPR Email Reader
Description: Please set a link using the slug cpr-email-list to get access
Version:     1.1.20170000070026
Author:      Feudy
Text Domain: wporg
Domain Path: /languages
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
global $cpremaillistreader_db_version;
$cpremaillistreader_db_version = '1.1.20170000070026';

function cpremaillistreader_install () {
	if (!isset($wpdb)) $wpdb = $GLOBALS['wpdb'];
	//global $wpdb;
	global $cpremaillistreader_db_version;
	
	add_option( 'cpremaillistreader_db_version', $cpremaillistreader_db_version );	
}

register_activation_hook( __FILE__, 'cpremaillistreader_install' );

function cpremaillistreader_update_db_check() {
    global $cpremaillistreader_db_version;
    if ( get_site_option( 'cpremaillistreader_db_version' ) != $cpremaillistreader_db_version ) {
        cpremaillistreader_install();
    }
}
add_action( 'plugins_loaded', 'cpremaillistreader_update_db_check' );

add_action( 'admin_menu', 'cpremaillistreader_menu' );

function cpremaillistreader_menu() {
	add_options_page( 'cpremaillistreader System Options', 'cpremaillistreader System', 'manage_options', 'cpremaillistreader', 'cpremaillistreader_options' );
}

function cpremaillistreader_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	fill_data();
}

?>
	<!--<script src="<?php //echo plugin_dir_url(dirname(__FILE__)).'cpr-email-list-reader/js/settings.js'; ?>"></script>-->
<?php 

add_action('init', function() {
	$url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');

	if ($url_path == 'cpr-email-list' OR $url_path == 'ilvestudio/cpr-email-list'){
		$template = dirname(__FILE__) . "/tmps/MyCPRDataReaderTemplate.php";
		if( $template )	{
			include( $template );
			load_template($template);
			exit();
		} 

	    $load = locate_template('MyCPRDataReaderTemplate.php', true);
	    if ($load) {
	        exit(); // just exit if template was found and loaded
		}
	}
});

function cpremaillistreader_styles() {
	wp_register_style( 'style', plugin_dir_url(dirname(__FILE__)).'cpr-email-list-reader/style.css' );
	wp_enqueue_style( 'style', plugin_dir_url(dirname(__FILE__)).'cpr-email-list-reader/style.css' );
}
add_action( 'wp_enqueue_scripts', 'cpremaillistreader_styles' );

?>
