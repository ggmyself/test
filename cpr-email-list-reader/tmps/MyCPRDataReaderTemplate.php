<?php /* Template Name:CPR Data Reader Template */ ?>

<?php get_header(); ?>


<div id="primary" class="content-area">

	<div class="content-page">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</div>
	<hr />
	<main id="main" class="site-main" role="main">
		
		<div class="main_container " id="main_div_container">
			<div class="input_row">Select Master Sheet: <input type="file" class="file_input" id="master_file" accept=".txt"></div>
			<div id="main_headers"></div>
			<div class="separatordiv"></div>
			<div class="input_row">Select List Sheet: <input type="file" class="file_input" id="list_file" accept=".txt,.tsv"></div>
			<!--<div class="form_input_button">	
				<div class="div_add_site_input_button"><input type="button" class="add_site_input_button" id="btnsubmit" value="Submit" /></div>
				<div class="div_add_site_input_button"><input type="button" class="add_site_input_button" id="btnaddissue" value="Add Issues" /></div>
				<div class="div_add_site_input_button"><input type="button" class="add_site_input_button" id="btncancel" value="Cancel" /></div>
			</div>-->
		</div>
		<div id="cover"><div id="urgent"></div></div>
		
 	</main><!-- .site-main -->
</div>
<script src="<?php echo plugin_dir_url(dirname(__FILE__)).'js/CPRReaderTemplate.js'; ?>"></script>

<?php get_sidebar( 'content-bottom' ); ?> 

<!--</div> .content-area -->

<?php get_footer(); ?>