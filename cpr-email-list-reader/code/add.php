<?php

	require_once('../../../../wp-load.php');

$data =$_POST['preparedData'] ;

if (empty($data)) {
	return "No data to save.";
}

$datatype = $data[0];
$data2 = $datatype;

if (($pos = strpos($datatype, "@"))>0) {
		$pos2 = strlen($datatype)-$pos;
		$phrase = substr($datatype, $pos+1);
		$datatype = substr($datatype, 0, $pos);
}

//var_dump($data);

if ($datatype == "add_new_site") {
	$url = $data[1];
	$name = $data[2];
	$address = $data[3];
	$city = $data[4];
	$postal = $data[5];
	$state = $data[6];
	$country = $data[7];
	$phone = $data[8];
	$website = $data[9];
	$email = $data[10];
	$fax = $data[11];
	/*$latitude = $data[12];
	$longitude = $data[13];*/
	$id = getNext("lstngslocation", "id");
	if ($id == false) {
		$id = rand(10000,100000);
	}

	//echo $fax;
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE url LIKE '%$url%' ")) {
	 	echo "The site already exists.";	 	return false;
	}
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE name LIKE '%$name%' and state LIKE '%$state%' ")) {
	 	echo "There is an existing location with that name in the same state.";	 	return false;
	}
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE phone LIKE '%$phone%' ")) {
	 	echo "There is an existing location with that phone number.";	 	return false;
	}
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE email LIKE '%$email%' ")) {
	 	echo "There is an existing location with that email address.";	 	return false;
	}	



	if (addToDb(" INSERT INTO `lstngslocation` (`id`, `url`, `name`, `phone`, `address`, `city`, `postal`, `state`, `country`, `website`, `email`, `fax`) VALUES ('$id', '$url', '$name', '$phone', '$address', '$city', '$postal', '$state', '$country', '$website', '$email', '$fax') ")) {
		echo $id;
		return true;
	} else {
		echo "There was a problem. Contact Support";
		return false;
	}
	
}

if ($datatype == "edit_site") {
	$url = $data[1];
	$name = $data[2];
	$address = $data[3];
	$city = $data[4];
	$postal = $data[5];
	$state = $data[6];
	$country = $data[7];
	$phone = $data[8];
	$website = $data[9];
	$email = $data[10];
	$fax = $data[11];
/*	$latitude = $data[12];
	$longitude = $data[13];*/
	$id = $phrase;
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE url LIKE '%$url%' AND id NOT LIKE '%$id%' ")) {
		echo "There is a diferent site using this url.";	 	
	 	return false;
	}
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE name LIKE '%$name%' and state LIKE '%$state%' AND id NOT LIKE '%$id%' ")) {
	 	echo "There is a diferent location with that name in the same state.";	 	return false;
	}
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE phone LIKE '%$phone%' AND id NOT LIKE '%$id%' ")) {
	 	echo "There is a diferent location with that phone number.";	 	return false;
	}
	if (searchIfExists("SELECT url, name, address, phone FROM lstngslocation WHERE email LIKE '%$email%' AND id NOT LIKE '%$id%' ")) {
	 	echo "There is a diferent location with that email address.";	 	return false;
	}	
		
	$connection = connectToDb();
	if ($connection != false) {
		$results = $connection->get_results(
			$connection->prepare("UPDATE `lstngslocation` SET `url` = %s, `name` = %s, `phone` = %s, `address` = %s, `city` = %s, `postal` = %s, `state` = %s, `country` = %s, `website` = %s, `email` = %s, `fax` = %s WHERE `id` LIKE %d ", $url, $name, $phone, $address, $city, $postal, $state, $country, $website, $email, $fax, $id));		
		
			echo true;
			return true;
	}
}


if ($datatype == "add_listings") {
//	try {
	$size = sizeof($data);
	$url = $data[1];
	//$urlid = $data[2]; 
	if (($pos = strpos($url, "@"))>0) {
		$pos2 = strlen($url)-$pos;
		$urlid = substr($url, $pos+1);
		$url = substr($url, 0, $pos);
	}


	$issueArray = [];
	//$alreadyOnDB ="";
	for ($i = 2; $i < $size; $i++) {
		$datos = split('@#@',$data[$i]);
		if ($i%2==0) {
			$id = "$datos[0]";
			$listing = $datos[1];
			if (strlen($listing)>0) {
				//echo $listing."\n";
				if (searchIfExists("SELECT url, listings FROM lstngslisting WHERE url LIKE '%$url%' and listings LIKE '%$listing%' ")) {
					$id = "";
					$listing ="";
					//echo "found";
				} else {
					if (addToDb(" INSERT INTO `lstngslisting` (`id`, `urlid`, `url`, `listings`) VALUES ('$id', '$urlid', '$url', '$listing')")) {

					} else {
						echo "There was a problem. Contact Support ID: ".$id." Listing: ".$listing." ".$i."\n";
						echo $url."\n";
						//searchIfExists("SELECT url, listings from lstngslisting WHERE url LIKE '%$url%' AND listings LIKE $listing")
					}
				}
			}
		} else {
			if ($id!="") {
				$issues = split(', ',$datos[1]);
				$long = sizeof($issues);
				for ($j=0; $j <= $long ; $j++) { 
					$issue = strtoupper($issues[$j]);
					//echo $issue."\n";/*
					if (strlen($issue)>0) {
						//$issueArray[] = $issue;
							updateIssues($issue);
						//echo $issue."\n";
						if (addToDb(" INSERT INTO `lstngsissue` (`id`, `urlid`, `url`, `listing`, `problem`) VALUES ('$id', '$urlid', '$url', '$listing', '$issue')")) {

						} else {
							echo "There was a problem. Contact Support ID: ".$id." Issue: ".$issue." ".$i."\n";
							//searchIfExists("SELECT url, listings from lstngslisting WHERE url LIKE '%$url%' AND listings LIKE $listing");
						}
					}
				}
			}
		}
	}
}

if ($datatype == "update_issues_status") {

	$url = $phrase;

	$mlong = sizeof($data);
	$today = date("Y-m-d H:i:s");

	for ($x=1; $x <= $mlong ; $x++) { 

		$details = preg_split('/9998999/',$data[$x]);
//		$details = split('#@#',$data[$x]);
		//$details = split(', ',$datos[1]);
//var_dump($data[$x]);
		$long = sizeof($details);
		//for ($j=1; $j <= $long ; $j++) {
		$idurl = $details[0];
		$idsue = "";
		if (($pos = strpos($idurl, "@"))>0) {
				$pos2 = strlen($idurl)-$pos;
				$idsue = substr($idurl, $pos+1);
				$idurl = substr($idurl, 0, $pos);
		}
		$url = $details[1];
		$issue = $details[2];
		$option = $details[3];
		$date = $details[4];

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//($table, $field, $field2, $conditions
			$connection = connectToDb();
		if ($connection != false) {
		//	$results = $connection->get_results(
			//				$connection->prepare("SELECT status as field, urlid as field2 FROM lstngsissue WHERE id LIKE %s AND problem LIKE %s ", $idurl, $issue));
							
			$results = $connection->get_results(
						$connection->prepare("SELECT `status` as field, `urlid` as field2 FROM lstngsissue WHERE id LIKE %s AND problem LIKE %s ", $idurl, $issue), ARRAY_A );
	
			$prev_stat  = $results[0];
			$stat_prev = $prev_stat['field'];
			$id_prev = $prev_stat['field2'];
			
					addToDb(" UPDATE `lstngsissue` SET `status` = '$option', `solveddate` = '$today', `listing` = '$url'  WHERE `id` LIKE '$idurl' AND `problem` LIKE '$issue' ");
		addToDb(" INSERT INTO `updtsissues` (`id`, `urlid`, `problem`, `pre_status`, `new_status`, `update_date`) VALUES ('$idurl', '$id_prev', '$idsue', '$stat_prev', '$option', '$today')");
			
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////	
				
		}
	}
	//return;
}

function updateIssues($issue) {
	if (searchIfExists("SELECT issue FROM issues_data WHERE issue LIKE '%$issue%' ")) {
	} else { 
		addToDb(" INSERT INTO `issues_data` (`issue`) VALUES ('$issue')");
	}
}

function connectToDb() {
	global $wpdb;
	$myNewDb = new wpdb('root', '', 'lstingsmnagment', 'localhost');
	if ($myNewDb) {
			return $myNewDb;	
	} else {
		//Error conecting to DB//
		return false;
	}
}

function searchIfExists($sqlstr) {
	$connection = connectToDb();
	if ($connection != false) {
		$results = $connection->get_results($sqlstr);
		if ($results) {
			return true;
		} else {
			return false;
		}
		@mysql_close ( $connection->dbh );
	}
}

function getNext($table, $field) {
	$max = 0;
	if (($table) && ($field)) {
		$connection = connectToDb();
		if ($connection != false) {
			$sqlstr = "SELECT MAX($field) as max_id FROM $table ";
			$results = $connection->get_results($sqlstr) or die (mysql_error('lstingsmnagment'));
			if ($results) {
				$max = $results[0]->max_id + 1;		
			} else {
				$max = 1;
			}
		}
	}
	return $max;
}

function getStatus($table, $field, $field2, $conditions) {
	$val = false;
	if (($table) && ($field)) {
		$connection = connectToDb();
		if ($connection != false) {
//			$sqlstr = "SELECT $field as field, $field2 as field2 FROM $table WHERE $conditions ";
//			$results = $connection->get_results($sqlstr) or die (mysql_error('lstingsmnagment'));

			//$sqlstr = ("SELECT %s as field, %s as field2 FROM %s WHERE %s ",$field, $field2, $table, $conditions);			
			$results = $connection->get_results(
				$connection->prepare("SELECT %s as field, %s as field2 FROM %s WHERE %s ", $field, $field2, $table, $conditions));
				var_dump($results);
			/*if ($results) {
				$val = $results[0]->field + 1;
			} else {
				$val = 0;
			}*/
		}
	}
	return $results;
}

function addToDb($sqlstr) {
	$connection = connectToDb();
	if ($connection != false) {
		/*$results = $connection->get_results(
			$connection->prepare($sqlstr)
		);*/
		
		$results = $connection->query($sqlstr) or die (mysql_error('lstingsmnagment'));
				//var_dump($results);
		if ($results) {
			return true;
		} else {
			return false;
		}
	}
		@mysql_close ( $connection->dbh );
}

?>
